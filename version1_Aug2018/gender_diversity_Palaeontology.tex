\documentclass[11pt]{article}

\usepackage{xspace}
\usepackage{color}
\usepackage{xcolor}
\usepackage{xargs}
\usepackage[margin=2.5cm]{geometry}
\usepackage{graphicx}  
\usepackage{rotating}  

\usepackage[round]{natbib}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{lineno}
%\linenumbers
\usepackage{caption} 
%\usepackage[parfill]{parskip}
%\addtolength{\footnotesep}{5mm}
\addtolength{\skip\footins}{2pc plus 5pt}


\newcommand{\treesim}{\textsc{TreeSim}\xspace}
\newcommand{\fossilsim}{\textsc{FossilSim}\xspace}
\newcommand{\ape}{\textsc{ape}\xspace}
\newcommand{\paleotree}{\textsc{paleotree}\xspace}
\newcommand{\revbayes}{\textsc{RevBayes}\xspace}
\newcommand{\newick}{\textsc{newick}\xspace}

%\geometry{top=35mm, bottom=30mm, left=30mm, right=30mm}

% formatting
%\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.5} 


\hyphenpenalty=1000

\title{Gender diversity in the Palaeontology journal}

%\author{Rachel C. M. Warnock$^{1,2}$}
\date{}		

\begin{document}

\maketitle

\vspace*{-5em} 

%\noindent $^1$Department of Biosystems Science \& Engineering, Eidgen\"{o}ssische Technische Hochschule Z\"{u}rich, 4058 Basel, Switzerland;\\
%$^2$Swiss Institute of Bioinformatics (SIB), Switzerland\\

%\noindent{\bf Correspondence:} Rachel Warnock, E-mail: rachel.warnock@bsse.ethz.ch.\\ 

%\section*{Abstract}

%\textbf{Key words} : fossil preservation, sampling biases, phylogeny, simulation, macroevolution

\section{Overview of findings}

A summary of gender diversity among authors in the Palaeontology journal is presented in Figs. \ref{fig1}--\ref{fig3} and Tables \ref{table1}--\ref{table2}.
The data indicate that the proportion of female authors contributing to the journal has not changed and remains approximately 0.2.
The proportion of first authors that are female and the proportion of articles that have $\textgreater 0.5$ female co-authorship also remains unchanged. 
The proportion of articles that have no female authors is decreasing but this partly reflects an increase in the average number of authors per article.

\section{Methods summary}

Author information was downloaded from the Palaeontology journal website\footnote{www.palass.org/publications/palaeontology-journal/} for all available articles, which span the interval the interval 1957--2018, using the R packages \texttt{xml2} and \texttt{Rvest}.
Gender was predicted based on first names using the R package \texttt{gender} using three of five available methods (= databases):

\begin{itemize}
\item \texttt{ssa} US Social Security Administration baby name data
\item \texttt{ipums} US Census data
\item \texttt{napp} Canada, Great Britain, Denmark, Iceland, Norway, Sweden census data from 1801 and 1910
\end{itemize}

Gender was assigned using the most frequent gender returned using the three methods and uncertainty (the proportion fe/males associated with a given name) was taken as the average across methods.
Names that could not be assigned to either gender above the 0.8 confidence level were treated as ``unknown''.
Most unknowns can be attributed to first names being represented in the database by initials only.
All code available to reproduce figures and tables is available online\footnote{https://bitbucket.org/rachelwarnock/diversity-study/}.

%Note: The `kantrowitz` method, which is based on the "Kantrowitz corpus", was excluded because this method doesn't return a measure of uncertainty and `genderize`, which uses data from social media profiles, was excluded because this method relies an API, which makes it quite slow. 

%\clearpage

\section{Data summary}

As of August the 8th 2018 the database contains 6528 individual author entries across 3231 unique articles, of which 4101 were assigned a gender above the 0.8 confidence level (male = 3346, female = 755). 
2155 articles have at least one gender assigned author, 1619 have 100\% gender assigned authors.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.75]{fig1}
\includegraphics[scale=0.75]{fig2}
\caption{Plots based on the complete dataset.}
\label{fig1}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.75]{fig3}
\includegraphics[scale=0.75]{fig4}
\caption{Plots based on the subset of names for which gender was assigned above the 0.8 level (i.e. ``unknowns'' are excluded from the data).}
\label{fig2}
\end{figure}

\begin{sidewaysfigure}[h!]
%\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{fig5}
\includegraphics[scale=0.6]{fig6}
\includegraphics[scale=0.6]{fig7}
\includegraphics[scale=0.6]{fig8}
\caption{Plots based on articles that have 100\% gender assigned authors above the 0.8 level.}
\label{fig3}
%\end{figure}
\end{sidewaysfigure}

%\begin{figure}[h!]
%\centering
%\includegraphics[scale=0.75]{fig7}
%\includegraphics[scale=0.75]{fig8}
%\label{speciation}
%\end{figure}

%\clearpage
%\bibliographystyle{mee} %plainnat
%\bibliography{revbayes_ref}

\newpage

\begin{table}[htb]
\centering
\caption{\small Average number of male, female, unknown and proportion of female authors for each six year interval}
\label{table1}
\begin{tabular}{c c c c c}
\hline
Interval & male & female & unknown & $P_f$ \\
\hline
1957--1963 & 8.4 & 2.3 & 28.9 & 0.21 \\ 
1963--1969 & 15.7 & 3.7 & 39.3 & 0.19 \\ 
1969--1975 & 18.8 & 3.0 & 45.5 & 0.14 \\ 
1975--1981 & 21.2 & 4.5 & 38.8 & 0.18 \\ 
1981--1988 & 30.2 & 5.2 & 30.7 & 0.15 \\ 
1988--1994 & 35.2 & 7.8 & 34.3 & 0.18 \\ 
1994--2000 & 49.8 & 12.5 & 40.3 & 0.20 \\ 
2000--2006 & 76.5 & 18.5 & 43.2 & 0.19 \\ 
2006--2012 & 140.7 & 32.0 & 46.3 & 0.19 \\ 
2012--2018 & 137.0 & 30.9 & 44.9 & 0.18 \\ 
\hline
\end{tabular}
\end{table}

\begin{table}[htb]
\small
\centering
\caption{\footnotesize Total number of male, female, unknown and proportion of female authors for each year}
\label{table2}
\begin{tabular}{c c c c c}
\hline
Year & male & female & unknown & $P_f$ \\
\hline
1957 & 3 & 0 & 4 & 0 \\
1958 & 6 & 0 & 12 & 0 \\
1959 & 4 & 0 & 23 & 0 \\
1960 & 9 & 2 & 34 & 0.18 \\
1961 & 9 & 5 & 31 & 0.36 \\
1962 & 8 & 4 & 38 & 0.33 \\
1963 & 20 & 5 & 60 & 0.2 \\
1964 & 9 & 1 & 36 & 0.1 \\
1965 & 16 & 4 & 45 & 0.2 \\
1966 & 15 & 5 & 30 & 0.25 \\
1967 & 12 & 5 & 34 & 0.29 \\
1968 & 26 & 5 & 48 & 0.16 \\
1969 & 16 & 2 & 43 & 0.11 \\
1970 & 14 & 5 & 41 & 0.26 \\
1971 & 22 & 3 & 38 & 0.12 \\
1972 & 20 & 1 & 38 & 0.05 \\
1973 & 16 & 1 & 51 & 0.06 \\
1974 & 21 & 5 & 48 & 0.19 \\
1975 & 20 & 3 & 57 & 0.13 \\
1976 & 14 & 6 & 38 & 0.3 \\
1977 & 14 & 2 & 32 & 0.12 \\
1978 & 34 & 8 & 45 & 0.19 \\
1979 & 26 & 3 & 41 & 0.1 \\
1980 & 27 & 5 & 38 & 0.16 \\
1981 & 12 & 3 & 39 & 0.2 \\
1982 & 29 & 7 & 34 & 0.19 \\
1983 & 22 & 3 & 39 & 0.12 \\
1984 & 33 & 5 & 37 & 0.13 \\
1985 & 34 & 4 & 25 & 0.11 \\
1986 & 37 & 5 & 23 & 0.12 \\
1987 & 26 & 7 & 26 & 0.21 \\
\hline
\end{tabular}
\quad
\vrule
\quad
\quad
\begin{tabular}{c c c c c}
\hline
Year & male & female & unknown & $P_f$ \\
\hline
1988 & 35 & 5 & 37 & 0.12 \\
1989 & 25 & 8 & 30 & 0.24 \\
1990 & 37 & 8 & 30 & 0.18 \\
1991 & 38 & 8 & 37 & 0.17 \\
1992 & 30 & 3 & 42 & 0.09 \\
1993 & 46 & 15 & 30 & 0.25 \\
1994 & 21 & 6 & 43 & 0.22 \\
1995 & 51 & 13 & 55 & 0.2 \\
1996 & 46 & 14 & 32 & 0.23 \\
1997 & 59 & 15 & 34 & 0.2 \\
1998 & 61 & 11 & 45 & 0.15 \\
1999 & 61 & 16 & 33 & 0.21 \\
2000 & 67 & 11 & 24 & 0.14 \\
2001 & 63 & 21 & 37 & 0.25 \\
2002 & 82 & 10 & 36 & 0.11 \\
2003 & 71 & 15 & 42 & 0.17 \\
2004 & 99 & 26 & 57 & 0.21 \\
2005 & 77 & 28 & 63 & 0.27 \\
2006 & 114 & 28 & 37 & 0.2 \\
2007 & 146 & 32 & 54 & 0.18 \\
2008 & 161 & 39 & 51 & 0.2 \\
2009 & 138 & 27 & 43 & 0.16 \\
2010 & 133 & 27 & 51 & 0.17 \\
2011 & 152 & 39 & 42 & 0.2 \\
2012 & 161 & 38 & 51 & 0.19 \\
2013 & 129 & 41 & 46 & 0.24 \\
2014 & 140 & 29 & 39 & 0.17 \\
2015 & 141 & 26 & 36 & 0.16 \\
2016 & 138 & 25 & 49 & 0.15 \\
2017 & 135 & 38 & 61 & 0.22 \\
2018 & 115 & 19 & 32 & 0.14 \\
\hline
\end{tabular}
\end{table}


\end{document}




